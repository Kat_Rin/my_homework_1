class Cup:
    def __init__(self, r, v):  
        self.radius = r  
        self.volume = v
    def display(self):
        print('Radius: {}, Volume: {}'.format(self.radius, self.volume))
    def set_radius(self, value):
        self.radius = value
    def set_volume(self, value):
        self.volume = value
    def display_summa(self):
        print('Summa: {}'.format(self.radius + self.volume))
    def display_max_value(self):
        print('MaxValue: {}'.format(max(self.radius, self.volume)))
    
cup1 = Cup(3, 5)
cup1.display()

cup1.set_radius(9)
cup1.set_volume(10)
cup1.display()

cup1.display_summa()

cup1.display_max_value()

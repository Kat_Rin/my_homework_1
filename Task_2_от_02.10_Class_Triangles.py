class Triangle:
    def __init__(self, ax, ay, bx, by, cx, cy):
        self.Ax = ax
        self.Ay = ay
        self.Bx = bx
        self.By = by
        self.Cx = cx
        self.Cy = cy
        import math
        self.A = math.sqrt(math.pow(bx - ax, 2) + math.pow(by - ay, 2))
        self.B = math.sqrt(math.pow(cx - bx, 2) + math.pow(cy - by, 2))
        self.C = math.sqrt(math.pow(ax - cx, 2) + math.pow(ay - cy, 2))
    def print_square(self):
        p = (self.A + self.B + self.C)/2
        import math
        s = math.sqrt(p * (p-self.A) * (p-self.B) * (p-self.C))
        print('Площадь: {}'.format(s))
    def print_perimeter(self):
        p = (self.A + self.B + self.C)
        print('Периметр: {}'.format(p))
    def print_mediana_point(self):
        mx = (self.Ax + self.Bx + self.Cx) / 3
        my = (self.Ay + self.By + self.Cy) / 3
        print('Точка пересечения медиан: ({};{})'.format(mx, my))
    def print_sost(self):
        if self.A == self.B or self.A == self.C or self.C == self.B:
            print('Это равнобедренный треугольник')
        elif self.A == self.B and self.B == self.C:
            print('Это равносторонний треугольник')
        else:
            print('Это произвольный треугольник')
tr1 = Triangle(3, 4, 5, 4, 1, 7)
tr1.print_square()
tr1.print_perimeter()
tr1.print_mediana_point()
tr1.print_sost()
        

